use rand::Rng;

// size of the map
pub const MAP_WIDTH: i32 = 80;
pub const MAP_HEIGHT: i32 = 45;

//room_param
pub const MAX_ROOMS: i32 = 15;
pub const MIN_ROOMS: i32 = 5;

pub const ROOM_MAX_SIZE: i32 = 10;
pub const ROOM_MIN_SIZE: i32 = 5;

use std::cmp;

#[derive(Clone, Copy, Debug)]
pub struct Rect {
    pub x1: i32,
    pub y1: i32,
    pub x2: i32,
    pub y2: i32,
    pub xm: i32, //middle x
    pub ym: i32, //middle y
}

impl Rect {
    pub fn new(x: i32, y: i32, w: i32, h: i32) -> Self {
        Rect {
            x1: x,
            y1: y,
            x2: x + w,
            y2: y + h,
            xm: x + w / 2,
            ym: y + h / 2
        }
    }
}
pub fn create_random_room(map: &mut Map) -> Rect {
    let room = Rect::new(rand::thread_rng()
                                .gen_range(0..(MAP_WIDTH - ROOM_MAX_SIZE)),
                         rand::thread_rng()
                                .gen_range(0..(MAP_HEIGHT - ROOM_MAX_SIZE)),
                         rand::thread_rng()
                                .gen_range(ROOM_MIN_SIZE..ROOM_MAX_SIZE),
                         rand::thread_rng()
                                .gen_range(ROOM_MIN_SIZE..ROOM_MAX_SIZE));
    create_room(room, map);
    room
}

fn create_room(room: Rect, map: &mut Map) {
    // go through the tiles in the rectangle and make them passable
    for x in (room.x1 + 1)..room.x2 {
        for y in (room.y1 + 1)..room.y2 {
            map[x as usize][y as usize] = Tile::empty();
        }
    }
}

pub fn create_tunnel(x1: i32, x2: i32, y1: i32, y2: i32, map: &mut Map){ // created tunnel by vertical and horizontal with corner, if it's nessasary
    if &x1 == &x2 {
        create_tunnel_v(y1, y2, x1, map);       
    }
    else if &y1 == &y2 {
        create_tunnel_h(x1, x2, y1, map);
    }
    else{
        create_tunnel_v(y1, y2, x1, map);
        create_tunnel_h(x1, x2, y2, map);
    }}


fn create_tunnel_h(x1: i32, x2: i32, y: i32, map: &mut Map) { // created tunnel by horizontal
    for x in cmp::min(x1, x2)..(cmp::max(x1, x2) +1) {
        map[x as usize][y as usize] = Tile::empty();
    }
}

fn create_tunnel_v(y1: i32, y2: i32, x: i32, map: &mut Map) { // created tunnel by vertical
    for y in cmp::min(y1, y2)..(cmp::max(y1, y2) +1) {
        map[x as usize][y as usize] = Tile::empty();
    }
}

pub fn make_map(room_list: &mut Vec<Rect>) -> Map {
    // fill map with "blocked" tiles
    let mut map = vec![vec![Tile::wall(); MAP_HEIGHT as usize]; MAP_WIDTH as usize];

    // create two rooms
    room_list.push(create_random_room(&mut map));

    //create_tunnel(30, 50, 25, 30, &mut map);
    
    let mut rng = rand::thread_rng();
    for i1 in 1..(rng.gen_range(MIN_ROOMS..MAX_ROOMS) as i32){
        room_list.push(create_random_room(&mut map));
        let i:usize = i1 as usize;
        create_tunnel(room_list[i-1].xm , room_list[i].xm ,
                      room_list[i-1].ym , room_list[i].ym ,
                      &mut map);
    }
    map
}
/// A tile of the map and its properties
#[derive(Clone, Copy, Debug)]
pub struct Tile {
    pub blocked: bool,
    pub block_sight: bool,
    pub visible: bool
}

impl Tile {
    pub fn empty() -> Self {
        Tile {
            blocked: false,
            block_sight: false,
            visible: false
        }
    }

    pub fn wall() -> Self {
        Tile {
            blocked: true,
            block_sight: true,
            visible: false
        }
    }

    pub fn is_empty(self) -> bool {
        (self.blocked == false) & (self.block_sight == false) & (self.visible == false)
    }
}

pub type Map = Vec<Vec<Tile>>;

pub struct Game {
    pub map: Map,
}

impl Game {
    pub fn new() -> Game{
        Game { map: Vec::new() }
    }

    pub fn print_map(self){
        for row in self.map{
            for tile in row{
                print!("{}", if tile.is_empty() {"# "} else {"  "})
            }
            println!("");
        }
    }
}