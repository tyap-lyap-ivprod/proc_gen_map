pub mod map_gen;

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}

/*

use proc_map_gen::map_gen;

fn main(){
    let mut room_list: Vec<map_gen::Rect> = Vec::new();
    let game = map_gen::Game {map: map_gen::make_map(&mut room_list)};
    game.print_map();
}

*/